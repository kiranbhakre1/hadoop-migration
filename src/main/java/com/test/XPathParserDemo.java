package com.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.apache.commons.collections.IteratorUtils;
import org.saama.informatica.SourceField;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class XPathParserDemo {
    @Nullable
    @Deprecated
    @NotNull

    public static void main(String[] args) {
        try {
            File inputFile = new File("Resources/small.XML");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            ArrayList<SourceField> CoulmnList = new ArrayList<SourceField>();

            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            XPath xPath = XPathFactory.newInstance().newXPath();


            String expression = "/POWERMART/REPOSITORY/FOLDER/SOURCE";
            NodeList SourceList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < SourceList.getLength(); i++) {
                Node nNode = SourceList.item(i);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element columnList = (Element) nNode;

                    for (int j = 0; j < columnList.getElementsByTagName("SOURCEFIELD").getLength(); j++) {
                        SourceField Source = null;
                        NamedNodeMap Columnattr = columnList.getElementsByTagName("SOURCEFIELD").item(j).getAttributes();

                        //IteratorUtils.toListIterator(Columnattr);
                        System.out.println("SOURCEFIELD : " + Columnattr.getNamedItem("FIELDNUMBER"));
                        String A="";
                       A=Columnattr.getNamedItem("FIELDNUMBER").getNodeName();


                       // Source.setBUSINESSNAME(A);



                        CoulmnList.add(Source);



                    }
                }
                System.out.println(nNode.getNodeName());
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }
}
