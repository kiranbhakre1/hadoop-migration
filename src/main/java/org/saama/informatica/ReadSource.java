package org.saama.informatica;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.saama.informatica.SourceField;

public class ReadSource {
    public static void main(String[] args) {

        try {
            File inputFile = new File("Resources/Big.XML");
            SAXReader reader = new SAXReader();
            Document document = reader.read(inputFile);

            System.out.println("Root element :"
                    + document.getRootElement().getName());

            List <Source> sources = new ArrayList<Source>();
            List<SourceField> SourceFields = new ArrayList<SourceField>();
            List<Node> SourceList = document.selectNodes("/POWERMART/REPOSITORY/FOLDER/SOURCE");

            for (int i = 0; i < SourceList.size(); i++) {
                Source source= new Source();
                Node nNode = SourceList.get(i);
                List<Node> cList = nNode.selectNodes("SOURCEFIELD");
                Element source_element = (Element) SourceList.get(i);

                for (int j = 0; j < cList.size(); j++) {
                    SourceField sourceField = new SourceField();
                    Element element = (Element) cList.get(j);
                    sourceField.setBUSINESSNAME(element.attributeValue("BUSINESSNAME"));
                    sourceField.setDATATYPE(element.attributeValue("DATATYPE"));
                    sourceField.setDESCRIPTION(element.attributeValue("DESCRIPTION"));
                    sourceField.setFIELDNUMBER(element.attributeValue("FIELDNUMBER"));
                    sourceField.setFIELDPROPERTY(element.attributeValue("FIELDPROPERTY"));
                    sourceField.setFIELDTYPE(element.attributeValue("FIELDTYPE"));
                    sourceField.setHIDDEN(element.attributeValue("HIDDEN"));
                    sourceField.setKEYTYPE(element.attributeValue("KEYTYPE"));
                    sourceField.setLENGTH(element.attributeValue("LENGTH"));
                    sourceField.setLEVEL(element.attributeValue("LEVEL"));
                    sourceField.setNAME(element.attributeValue("NAME"));
                    sourceField.setNULLABLE(element.attributeValue("NULLABLE"));
                    sourceField.setOCCURS(element.attributeValue("OCCURS"));
                    sourceField.setOFFSET(element.attributeValue("OFFSET"));
                    sourceField.setPHYSICALLENGTH(element.attributeValue("PHYSICALLENGTH"));
                    sourceField.setPHYSICALOFFSET(element.attributeValue("PHYSICALOFFSET"));
                    sourceField.setPICTURETEXT(element.attributeValue("PICTURETEXT"));
                    sourceField.setPRECISION(element.attributeValue("PRECISION"));
                    sourceField.setSCALE(element.attributeValue("SCALE"));
                    sourceField.setUSAGE_FLAGS(element.attributeValue("USAGE_FLAGS"));
                    SourceFields.add(sourceField);
                }
                source.setBUSINESSNAME(source_element.attributeValue("BUSINESSNAME"));
                source.setDATABASETYPE(source_element.attributeValue("DATABASETYPE"));
                source.setDBDNAME(source_element.attributeValue("DBDNAME"));
                source.setDESCRIPTION(source_element.attributeValue("DESCRIPTION"));
                source.setNAME(source_element.attributeValue("NAME"));
                source.setOBJECTVERSION(source_element.attributeValue("OBJECTVERSION"));
                source.setOWNERNAME(source_element.attributeValue("OWNERNAME"));
                source.setVERSIONNUMBER(source_element.attributeValue("VERSIONNUMBER"));
                source.setColumns(SourceFields);
                sources.add(source);
            }

            System.out.println(sources.toString());

        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
}
