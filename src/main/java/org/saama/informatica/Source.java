package org.saama.informatica;

import java.util.ArrayList;
import java.util.List;

public class Source
{

   private String BUSINESSNAME;
   private String DATABASETYPE;
   private String DBDNAME;
   private String DESCRIPTION;
   private String NAME;
   private String OBJECTVERSION;
   private String OWNERNAME;
   private String VERSIONNUMBER;
   private List<SourceField> columns;

    /**
     * @return
     */
    @Override
    public String toString()
    {
        return getBUSINESSNAME()+" "+getDATABASETYPE()+" "+getDBDNAME()+" "+getDESCRIPTION()+" "+getNAME()
                +" "+getOBJECTVERSION()+" "+getOWNERNAME()+" "+getVERSIONNUMBER()+" "+getColumns().toString()+ "\n";    }

    public String getBUSINESSNAME() {
        return BUSINESSNAME;
    }

    public void setBUSINESSNAME(String BUSINESSNAME) {
        this.BUSINESSNAME = BUSINESSNAME;
    }

    public String getDATABASETYPE() {
        return DATABASETYPE;
    }

    public void setDATABASETYPE(String DATABASETYPE) {
        this.DATABASETYPE = DATABASETYPE;
    }

    public String getDBDNAME() {
        return DBDNAME;
    }

    public void setDBDNAME(String DBDNAME) {
        this.DBDNAME = DBDNAME;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getOBJECTVERSION() {
        return OBJECTVERSION;
    }

    public void setOBJECTVERSION(String OBJECTVERSION) {
        this.OBJECTVERSION = OBJECTVERSION;
    }

    public String getOWNERNAME() {
        return OWNERNAME;
    }

    public void setOWNERNAME(String OWNERNAME) {
        this.OWNERNAME = OWNERNAME;
    }

    public String getVERSIONNUMBER() {
        return VERSIONNUMBER;
    }

    public void setVERSIONNUMBER(String VERSIONNUMBER) {
        this.VERSIONNUMBER = VERSIONNUMBER;
    }

    public List<SourceField> getColumns() {
        return columns;
    }

    public void setColumns(List<SourceField> columns) {
        this.columns = columns;
    }




}