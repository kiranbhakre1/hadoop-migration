package org.saama.informatica;

public class SourceField
{

   private String BUSINESSNAME;
   private String DATATYPE;
   private String DESCRIPTION;
   private String FIELDNUMBER;
   private String FIELDPROPERTY;
   private String FIELDTYPE;
   private String HIDDEN;
   private String KEYTYPE;
   private String LENGTH;
   private String LEVEL;
   private String NAME;
   private String NULLABLE;
   private String OCCURS;
   private String OFFSET;
   private String PHYSICALLENGTH;
   private String PHYSICALOFFSET;
   private String PICTURETEXT;
   private String PRECISION;
   private String SCALE;
   private String USAGE_FLAGS;

   @Override
   public String toString()
   {
      return getBUSINESSNAME() +" "+getDATATYPE()+" "+getDESCRIPTION()+ " "+ getFIELDNUMBER()+ " "+ getFIELDPROPERTY()+ " "+ getFIELDTYPE()+ " "+ getHIDDEN()+ " "
              +getKEYTYPE()+ " "+getLENGTH()+ " "+getLEVEL()+ " "+getNAME()+ " "+getNULLABLE()+ " "+getOCCURS()+ " "+getOFFSET()+ " "+getPHYSICALLENGTH()+ " "+getPHYSICALOFFSET()+ " "+getPICTURETEXT()
              + " "+getPRECISION()+ " "+getSCALE()+ " "+getUSAGE_FLAGS();

   }


    public String getBUSINESSNAME() {
        return BUSINESSNAME;
    }

    public void setBUSINESSNAME(String BUSINESSNAME) {
        this.BUSINESSNAME = BUSINESSNAME;
    }

    public String getDATATYPE() {
        return DATATYPE;
    }

    public void setDATATYPE(String DATATYPE) {
        this.DATATYPE = DATATYPE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getFIELDNUMBER() {
        return FIELDNUMBER;
    }

    public void setFIELDNUMBER(String FIELDNUMBER) {
        this.FIELDNUMBER = FIELDNUMBER;
    }

    public String getFIELDPROPERTY() {
        return FIELDPROPERTY;
    }

    public void setFIELDPROPERTY(String FIELDPROPERTY) {
        this.FIELDPROPERTY = FIELDPROPERTY;
    }

    public String getFIELDTYPE() {
        return FIELDTYPE;
    }

    public void setFIELDTYPE(String FIELDTYPE) {
        this.FIELDTYPE = FIELDTYPE;
    }

    public String getHIDDEN() {
        return HIDDEN;
    }

    public void setHIDDEN(String HIDDEN) {
        this.HIDDEN = HIDDEN;
    }

    public String getKEYTYPE() {
        return KEYTYPE;
    }

    public void setKEYTYPE(String KEYTYPE) {
        this.KEYTYPE = KEYTYPE;
    }

    public String getLENGTH() {
        return LENGTH;
    }

    public void setLENGTH(String LENGTH) {
        this.LENGTH = LENGTH;
    }

    public String getLEVEL() {
        return LEVEL;
    }

    public void setLEVEL(String LEVEL) {
        this.LEVEL = LEVEL;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getNULLABLE() {
        return NULLABLE;
    }

    public void setNULLABLE(String NULLABLE) {
        this.NULLABLE = NULLABLE;
    }

    public String getOCCURS() {
        return OCCURS;
    }

    public void setOCCURS(String OCCURS) {
        this.OCCURS = OCCURS;
    }

    public String getOFFSET() {
        return OFFSET;
    }

    public void setOFFSET(String OFFSET) {
        this.OFFSET = OFFSET;
    }

    public String getPHYSICALLENGTH() {
        return PHYSICALLENGTH;
    }

    public void setPHYSICALLENGTH(String PHYSICALLENGTH) {
        this.PHYSICALLENGTH = PHYSICALLENGTH;
    }

    public String getPHYSICALOFFSET() {
        return PHYSICALOFFSET;
    }

    public void setPHYSICALOFFSET(String PHYSICALOFFSET) {
        this.PHYSICALOFFSET = PHYSICALOFFSET;
    }

    public String getPICTURETEXT() {
        return PICTURETEXT;
    }

    public void setPICTURETEXT(String PICTURETEXT) {
        this.PICTURETEXT = PICTURETEXT;
    }

    public String getPRECISION() {
        return PRECISION;
    }

    public void setPRECISION(String PRECISION) {
        this.PRECISION = PRECISION;
    }

    public String getSCALE() {
        return SCALE;
    }

    public void setSCALE(String SCALE) {
        this.SCALE = SCALE;
    }

    public String getUSAGE_FLAGS() {
        return USAGE_FLAGS;
    }

    public void setUSAGE_FLAGS(String USAGE_FLAGS) {
        this.USAGE_FLAGS = USAGE_FLAGS;
    }

}